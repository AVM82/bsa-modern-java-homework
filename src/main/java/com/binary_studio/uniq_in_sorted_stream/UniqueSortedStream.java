package com.binary_studio.uniq_in_sorted_stream;

import java.util.stream.Stream;

public final class UniqueSortedStream {

	static Long rowId = null;

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(row -> !row.getPrimaryId().equals(rowId)).peek(row -> rowId = row.getPrimaryId());
	}

}
