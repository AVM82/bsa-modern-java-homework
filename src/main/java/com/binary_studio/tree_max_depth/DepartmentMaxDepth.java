package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.List;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		LinkedList<Node> nodesQueue = new LinkedList<>();
		Node node;
		int maxDeep = 0;
		if (rootDepartment == null) {
			return maxDeep;
		}
		nodesQueue.add(new Node(rootDepartment, maxDeep + 1));
		while ((node = nodesQueue.poll()) != null) {
			maxDeep = node.department == null ? maxDeep : Math.max(maxDeep, node.level);
			List<Department> subDepartments = node.department != null ? node.department.subDepartments : null;
			if (subDepartments != null) {
				for (Department item : subDepartments) {
					nodesQueue.add(new Node(item, node.level + 1));
				}
			}
		}
		return maxDeep;
	}

	static class Node {

		int level;

		Department department;

		Node(Department department, int level) {
			this.level = level;
			this.department = department;
		}

	}

}
