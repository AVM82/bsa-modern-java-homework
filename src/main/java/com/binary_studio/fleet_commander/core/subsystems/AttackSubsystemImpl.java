package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private final String name;

	private final PositiveInteger powerGridRequirments;

	private final PositiveInteger capacitorConsumption;

	private final PositiveInteger optimalSpeed;

	private final PositiveInteger optimalSize;

	private final PositiveInteger baseDamage;

	private AttackSubsystemImpl(String name, PositiveInteger powerGridRequirments, PositiveInteger capacitorConsumption,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {

		this.name = name.trim();
		this.powerGridRequirments = powerGridRequirments;
		this.capacitorConsumption = capacitorConsumption;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridRequirments;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		double sizeReductionModifier = getSizeReductionModifier(target);
		double speedReductionModifier = getSpeedReductionModifier(target);
		int damage = calculateDamage(sizeReductionModifier, speedReductionModifier);
		return PositiveInteger.of(damage);
	}

	@Override
	public PositiveInteger getDamage() {
		return this.baseDamage;
	}

	private int calculateDamage(double sizeReductionModifier, double speedReductionModifier) {
		double damage = this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier);
		return (int) Math.ceil(damage);
	}

	private double getSpeedReductionModifier(Attackable target) {
		double targetSpeed = target.getCurrentSpeed().value();
		return (targetSpeed <= this.optimalSpeed.value()) ? 1 : this.optimalSpeed.value() / (2 * targetSpeed);
	}

	private double getSizeReductionModifier(Attackable target) {
		double targetSize = target.getSize().value();
		return (targetSize >= this.optimalSize.value()) ? 1 : targetSize / this.optimalSize.value();
	}

	@Override
	public String getName() {
		return this.name;
	}

}
