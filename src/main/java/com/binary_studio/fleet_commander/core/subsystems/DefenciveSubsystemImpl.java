package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	public static final int UPPER_BOUND_DAMAGE = 95;

	private final String name;

	private final PositiveInteger powerGridConsumption;

	private final PositiveInteger capacitorConsumption;

	private final PositiveInteger impactReductionPercent;

	private final PositiveInteger shieldRegeneration;

	private final PositiveInteger hullRegeneration;

	private DefenciveSubsystemImpl(String name, PositiveInteger powerGridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) {
		this.name = name.trim();
		this.powerGridConsumption = powerGridConsumption;
		this.capacitorConsumption = capacitorConsumption;
		this.impactReductionPercent = impactReductionPercent;
		this.shieldRegeneration = shieldRegeneration;
		this.hullRegeneration = hullRegeneration;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powerGridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DefenciveSubsystemImpl(name, powerGridConsumption, capacitorConsumption, impactReductionPercent,
				shieldRegeneration, hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		PositiveInteger reductionPercent;
		if (this.impactReductionPercent.value() > UPPER_BOUND_DAMAGE) {
			reductionPercent = PositiveInteger.of(UPPER_BOUND_DAMAGE);
		}
		else {
			reductionPercent = this.impactReductionPercent;
		}

		double reducedDamage = incomingDamage.damage.value() * (1 - reductionPercent.value() / 100D);
		PositiveInteger roundUpReduceDamage = PositiveInteger.of((int) Math.ceil(reducedDamage));
		return roundUpReduceDamage.equals(incomingDamage.damage) ? incomingDamage
				: incomingDamage.changeDamage(roundUpReduceDamage);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegeneration, this.hullRegeneration);
	}

}
