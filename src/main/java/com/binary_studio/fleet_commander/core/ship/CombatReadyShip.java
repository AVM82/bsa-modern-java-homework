package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	private boolean isAttacked = false;

	private final DockedShip ship;

	private PositiveInteger capacitorAmount;

	public CombatReadyShip(DockedShip ship) {
		this.ship = ship;
		this.capacitorAmount = ship.getCapacitorAmount();
	}

	@Override
	public void endTurn() {
		this.capacitorAmount = PositiveInteger
				.of(this.capacitorAmount.value() + this.ship.getCapacitorRechargeRate().value());
		if (this.capacitorAmount.value() > this.ship.getCapacitorAmount().value()) {
			this.capacitorAmount = PositiveInteger.of(this.ship.getCapacitorAmount().value());
		}
		this.isAttacked = false;
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.ship.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.ship.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.ship.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Optional<AttackAction> attackAction = Optional.empty();
		if (this.isAttacked
				|| this.capacitorAmount.value() < this.ship.getAttackSubsystem().getCapacitorConsumption().value()) {
			return attackAction;
		}
		else {
			attackAction = Optional.of(new AttackAction(this.ship.getAttackSubsystem().getDamage(), this.ship::getName,
					target, () -> this.ship.getAttackSubsystem().getName()));
			this.capacitorAmount = PositiveInteger.of(
					this.capacitorAmount.value() - this.ship.getAttackSubsystem().getCapacitorConsumption().value());
		}
		this.isAttacked = true;
		return attackAction;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		attack = this.ship.getDefenciveSubsystem().reduceDamage(attack);
		Integer attackDamage = attack.damage.value();
		Integer shieldHP = this.ship.getCurrentShieldHP().value();
		Integer hullHP = this.ship.getCurrentHullHP().value();
		if (attackDamage > shieldHP && attackDamage > hullHP) {
			return new AttackResult.Destroyed();
		}
		if (attackDamage > shieldHP) {
			this.ship.setCurrentShieldHP(PositiveInteger.of(0));
			this.ship.setCurrentHullHP(PositiveInteger.of(hullHP - (attackDamage - shieldHP)));
		}
		else {
			this.ship.setCurrentShieldHP(PositiveInteger.of(shieldHP - attackDamage));
		}
		return new AttackResult.DamageRecived(attack.weapon, attack.damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.capacitorAmount.value() < this.ship.getDefenciveSubsystem().getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		RegenerateAction regenerateAction = this.ship.getDefenciveSubsystem().regenerate();
		this.capacitorAmount = PositiveInteger
				.of(this.capacitorAmount.value() - this.ship.getDefenciveSubsystem().getCapacitorConsumption().value());
		return Optional.of(new RegenerateAction(regenerateShield(regenerateAction.shieldHPRegenerated),
				regenerateHull(regenerateAction.hullHPRegenerated)));
	}

	private PositiveInteger regenerateHull(PositiveInteger hullHPRegenerated) {
		int currentHullHP = this.ship.getCurrentHullHP().value();
		int startHullHP = this.ship.getHullHP().value();
		currentHullHP = currentHullHP + hullHPRegenerated.value();
		if (currentHullHP > startHullHP) {
			hullHPRegenerated = PositiveInteger.of(hullHPRegenerated.value() - (currentHullHP - startHullHP));
		}
		return hullHPRegenerated;
	}

	private PositiveInteger regenerateShield(PositiveInteger shieldHPRegenerated) {
		int currentShieldHP = this.ship.getCurrentShieldHP().value();
		int startShieldHP = this.ship.getShieldHP().value();
		currentShieldHP = currentShieldHP + shieldHPRegenerated.value();

		if (currentShieldHP > startShieldHP) {
			shieldHPRegenerated = PositiveInteger.of(shieldHPRegenerated.value() - (currentShieldHP - startShieldHP));
		}

		return shieldHPRegenerated;
	}

}
