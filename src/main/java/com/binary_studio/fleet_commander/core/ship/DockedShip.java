package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip implements ModularVessel {

	private static final int DEFENSIVE_SUBSYSTEM = 0;

	private static final int ATTACK_SUBSYSTEM = 1;

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger currentShieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger currentHullHP;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private PositiveInteger powerGridOutput;

	private DefenciveSubsystem defenciveSubsystem;

	private AttackSubsystem attackSubsystem;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powerGridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.currentShieldHP = shieldHP;
		this.hullHP = hullHP;
		this.currentHullHP = hullHP;
		this.powerGridOutput = powerGridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getCapacitorAmount() {
		return this.capacitorAmount;
	}

	public PositiveInteger getCapacitorRechargeRate() {
		return this.capacitorRechargeRate;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public PositiveInteger getCurrentShieldHP() {
		return this.currentShieldHP;
	}

	public PositiveInteger getCurrentHullHP() {
		return this.currentHullHP;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public void setCapacitorAmount(PositiveInteger capacitorAmount) {
		this.capacitorAmount = capacitorAmount;
	}

	public void setCurrentShieldHP(PositiveInteger currentShieldHP) {
		this.currentShieldHP = currentShieldHP;
	}

	public void setCurrentHullHP(PositiveInteger currentHullHP) {
		this.currentHullHP = currentHullHP;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powerGridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		fit(subsystem, ATTACK_SUBSYSTEM);
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		fit(subsystem, DEFENSIVE_SUBSYSTEM);
	}

	private void fit(Subsystem subsystem, int typeSubsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			unmountSubSystem(typeSubsystem);
		}
		else {
			mountSubSystem(subsystem);
		}
	}

	private void reducePG(PositiveInteger value) throws InsufficientPowergridException {
		if (value.value() > this.powerGridOutput.value()) {
			throw new InsufficientPowergridException(value.value() - this.powerGridOutput.value());
		}
		this.powerGridOutput = PositiveInteger.of(this.powerGridOutput.value() - value.value());
	}

	private void mountSubSystem(Subsystem subsystem) throws InsufficientPowergridException {
		reducePG(subsystem.getPowerGridConsumption());
		if (subsystem instanceof DefenciveSubsystem) {
			this.defenciveSubsystem = (DefenciveSubsystem) subsystem;
		}
		if (subsystem instanceof AttackSubsystem) {
			this.attackSubsystem = (AttackSubsystem) subsystem;
		}
	}

	private void unmountSubSystem(int typeSubsystem) {
		switch (typeSubsystem) {
		case (DEFENSIVE_SUBSYSTEM): {
			increase(this.defenciveSubsystem.getPowerGridConsumption());
			this.defenciveSubsystem = null;
			break;
		}
		case (ATTACK_SUBSYSTEM): {
			increase(this.attackSubsystem.getPowerGridConsumption());
			this.attackSubsystem = null;
			break;
		}
		}
	}

	private void increase(PositiveInteger value) {
		this.powerGridOutput = PositiveInteger.of(this.powerGridOutput.value() + value.value());
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem == null && this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		return new CombatReadyShip(this);
	}

}
